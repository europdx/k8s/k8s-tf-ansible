SHELL := /bin/bash

.PHONY: all start destroy load-secrets tf-apply ansible-configure tf-destroy

all:

start: tf-apply ansible-configure

destroy: tf-destroy

#load-secrets:
#		source secrets.sh

tf-plan:
		cd tf && terraform init
		cd tf && terraform plan

tf-apply:
		cd tf && terraform init
		cd tf && terraform apply

ansible-configure:
		cd ansible && ansible-playbook --private-key=~/.ssh/tf-edirex/id_rsa --extra-vars "RANCHER_TOKEN=${ENV_RANCHER_TOKEN}" k8s-dynamic.yml

tf-destroy:
		cd tf && terraform init
		cd tf && terraform destroy
