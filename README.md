# K8s cluster deployment

Use following sequence to start cluster.
````
source secrets.txt
make tf-apply
make ansible-configure
````

## Post-install steps

To enable propper scheduling on worker nodes(you need to left one worker node untainted) use following commands:

````kubectl taint node <NODE-NAME> edirex=cbio:NoSchedule````

````kubectl label node <NODE-NAME> edirex=cbio````


In Rancher GUI use following settings.

Cluster -> system -> workloads -> nginx-ingress-controller
-> edit -> node sceduling -> advanced options -> Add tolerations:

````
node-role.kubernetes.io/controlplane        Equal       true       NoSchedule

node-role.kubernetes.io/etcd                Equal       true       NoExecute
````

## Configure MTU in Canal

https://github.com/rancher/rancher/issues/13984

````
kubectl -n kube-system edit configMap canal-config
````

## Install nfs provisoner

To enable persistance storage (via nfs) use following commands:

````
kubectl apply -f nfs/
````

To consume nfs storage use following example:

````
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: test-claim
  annotations:
    volume.beta.kubernetes.io/storage-class: "managed-nfs-storage"
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Mi
````
## Deploy cbio on demand things

3 x deploy v https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/kubernetes-docker
