terraform {
  backend "swift" {
    container         = "k8s_terraform_state"
    archive_container = "k8s_terraform-state-archive"
  }
}
