# Create compute objects
resource "openstack_compute_instance_v2" "k8s_cluster_worker" {
  count      = "${var.k8s_worker_node_count}"
  name       = "${var.k8s_cluster_name}-worker-${count.index + 1}"
  image_name = "${var.image_name}"
  flavor_name     = "${var.flavor_hpc_4cpu16ram}"
  key_pair        = "${var.key_pair}"
  security_groups = ["ingress_ssh", "ingress_icmp", "default", "allow_ingress_http_https"]

  metadata = {
    group        = "g_${var.k8s_cluster_name}_worker"
    ansible_user = "ubuntu"
  }

  network {
    name = "${var.network_name}"
  }
}

# Set DNS records
resource "dns_a_record_set" "a_record_k8s_cluster_worker" {
  count     = "${var.k8s_worker_node_count}"
  zone      = "${var.zone}"
  name      = "${element(openstack_compute_instance_v2.k8s_cluster_worker.*.name, count.index)}"
  addresses = ["${element(openstack_compute_instance_v2.k8s_cluster_worker.*.access_ip_v4, count.index)}"]
  ttl       = "${var.ttl}"
}
