provider "openstack" {
  #  application_credential_id     = "${var.application_credential_id}"
  #  application_credential_secret = "${var.application_credential_secret}"
  auth_url = "https://identity.cloud.muni.cz/v3"

  region = "brno1"
}

# Configure the DNS Provider
provider "dns" {
  update {
    server        = "147.251.4.41"
    transport     = "tcp"
    key_name      = "edirex.ics.muni.cz."
    key_algorithm = "hmac-md5"
    key_secret    = "${var.dns_key_secret}"
  }
}
